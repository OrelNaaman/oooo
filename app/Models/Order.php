<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
        protected $fillable = ['model','address','price','phone', 'delivery_date','user_id'];
        // Connections
       public function users(){
            return $this->belongsTo('App\Models\User','user_id');
        }
}
