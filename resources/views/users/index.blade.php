@extends('layouts.app')

@section('title','User list')

@section('content')
@if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif
<div><a class="badge badge-primary text-wrap " href="{{route('users.create')}}">Add new User</a></div>
        <h1>Users List</h1>
        @if($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
        @endif
        <div class="table-responsive">
            <table class="table align-items-center table-flush" >
            <tr>
                <th>id</th><th>Name</th><th>Email</th><th>Role</th><th>Created</th><th>Updated</th><th>Edit User</th><th>Delete User</th>
            </tr>
            <!-- table data -->
            @foreach($users as $user)
                <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>
                    @foreach ($user->roles as $role)
                        {{ $role->name }}
                    @endforeach
                </td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td><a class="btn btn-outline-dark" href="{{route('users.edit',$user->id)}}" role="button">Edit</a></td>
                <td><a class="btn btn-outline-danger" href="{{route('user.delete',$user->id)}}" role="button">Delete</a></td>
                </tr>
            @endforeach
    </table>
        </div>
@endsection