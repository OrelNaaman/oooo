@extends('layouts.sidebar')

@section('title','Orders')

@section('content')

@if(Session::has('notallowed'))
<div class='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>

@endif
         @if (Session::has('message'))
            <div class="alert alert-success">{{Session::get('message')}} </div>
        @endif

    <h1>Orders</h1>
    <div><a class="badge badge-primary text-wrap " href="{{ url('orders/create') }}">Add new order</a></div>

    <div class="table-responsive">
    <table class="table align-items-center table-flush" >
            <tr>
                <th>id</th><th>model</th><th>address</th><th>price</th><th>phone</th><th>delivery_date</th><th>user_id</th><th>Created</th><th>Updated</th><th>Edit Order</th><th>Delete Order</th>
            </tr>
        <!-- the table data -->
            @foreach($orders as $order)
            <tr>
            <td> {{$order->id}}</td>
            <td> {{$order->model}}</td>
            <td> {{$order->address}}</td>
            <td> {{$order->price}}</td>
            <td> {{$order->phone}}</td>
            <td> {{$order->delivery_date}}</td>
            <td> {{$order->users->name}}</td>
            <td> {{$order->created_at}}</td>
            <td> {{$order->updated_at}}</td>
            <td><a class="btn btn-outline-dark" href="{{route('orders.edit',$order->id)}}" role="button">Edit</a></td>
            <td><a class="btn btn-outline-danger" href="{{route('orders.delete',$order->id)}}" role="button">Delete</a></td>
        </tr>
            @endforeach
    </table>


@endsection
