<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('orders', 'App\Http\Controllers\OrderController@index');

Route::get('/', function () {
    return view('welcome');
});

// Route::get("orders", [Order::class.'index']);
Route::get('orders/create', 'App\Http\Controllers\OrderController@create');


Route::get('orders/delete/{id}','OrdersController@destroy')->name('order.delete');

Route::resource('users', 'UsersController')->middleware('auth');

Route::get('users/delete/{id}','UsersController@destroy')->name('user.delete');

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home');
