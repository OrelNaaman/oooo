<?php

use Illuminate\Database\Seeder;

class UserrolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userroles')->insert([
            [
                'user_id' => '1',
                'role_id' => '1',
                'created_at' => now(),
                'updated_at' => now()
            ],
            ]);
    }
}
